class Operaciones  implements InterfaceOperaciones {
	@Override
	public int sumar(int a,int b){
		return a+b;
	}
	@Override
	public int restar(int a,int b){
		return a-b;
	}
	@Override
	public int multiplicar(int a,int b){
		return  a*b;
	}
	@Override
	public double dividir(double a,double b){
		return a/b;
	}
	
}