import java.util.Scanner;
import java.io.*;

public class Principal {
	public static void main(String[] args) throws IOException{
		
		Scanner sc = new Scanner(System.in);
		Operaciones op = new Operaciones();
		String datos = sc.nextLine();
		datos += "," + sc.nextLine();
		sc.close();
		ClienteOperaciones co = new ClienteOperaciones();
		System.out.println("Resultados:\n"+co.enviarDatos(datos));	

	}
	
}