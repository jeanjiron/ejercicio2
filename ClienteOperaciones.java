import java.io.*;
import java.net.Socket;

public class ClienteOperaciones {

    public String enviarDatos(String vals)throws IOException{
    	Socket socket = new Socket("localhost",ServidorOperaciones.puerto);
    	InputStream is = socket.getInputStream();
    	BufferedReader br = new BufferedReader(new InputStreamReader(is));

    	PrintStream pw = new PrintStream(socket.getOutputStream());
    	
    	pw.println(vals);

    	String sum = br.readLine();
    	String res = br.readLine();
    	String mul = br.readLine();
    	String div = br.readLine();
    	pw.close();
        br.close();
        socket.close();
    	return  sum + "\n" + res + "\n"+ mul + "\n" + div;
    }
}