import java.net.ServerSocket;
import java.net.Socket;
import java.io.*;

public class ServidorOperaciones{
    public static final int puerto = 5000;

    public static void main(String args[])throws IOException{
        ServerSocket serverSocket =  new ServerSocket(puerto);
        
        Operaciones op = new Operaciones();
        String txtVals;
        int a;
        int b;
        double ad;
        double bd;

        Socket socketCliente;
        while(true){
            socketCliente = serverSocket.accept();
            BufferedReader br = new BufferedReader(new InputStreamReader(socketCliente.getInputStream()));

            txtVals = br.readLine();

            String []token = txtVals.split(",");
            a = Integer.parseInt(token[0]);
            b = Integer.parseInt(token[1]);
            ad = Double.parseDouble(token[0]);
            bd = Double.parseDouble(token[1]);

            PrintWriter pw = new PrintWriter(socketCliente.getOutputStream());

            pw. println(""+token[0]+"+"+token[1]+" = " + op.sumar(a,b));
            pw. println(""+token[0]+"-"+token[1]+" = " + op.restar(a,b));
            pw. println(""+token[0]+"*"+token[1]+" = " + op.multiplicar(a,b));
            pw. println(""+token[0]+"/"+token[1]+" = " + op.dividir(ad,bd));
            
            pw.close();
            br.close();
            socketCliente.close();
        }
    }
}